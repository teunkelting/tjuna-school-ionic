

angular.module('blog', ['ionic', 'ngStorage'])

// create a new factory
.factory ('StorageService', function ($localStorage) {

  $localStorage = $localStorage.$default({
    things: []
  });

  var _getAll = function () {
    return $localStorage.things;
  };

  var _add = function (thing) {
    $localStorage.things.push(thing);
  }

  var _remove = function (thing) {
    $localStorage.things.splice($localStorage.things.indexOf(thing), 1);
  }

  return {
    getAll: _getAll,
    add: _add,
    remove: _remove
  };
})

.controller('tjunaCtrl', function($scope, $ionicModal, StorageService) {

  $scope.things = StorageService.getAll();

  $scope.add = function (newThing) {
    StorageService.add(newThing);
    document.getElementById("newNote").value= "";
  };

  $scope.remove = function (thing) {
    StorageService.remove(thing);
  };

  $scope.articles = [
    { title:      'Collect coins', 
      comments:   '12 comments',
      time:       '3h ago',
      category:   'finance',
      color:      'blue',
      image:      'img/mountains.jpg'
    },

    { title: 'Eat mushrooms', 
      comments: '6 comments',
      time:       '3h ago',
      category:   'food',
      color:      'yellow',
      image:      'img/cover.jpg'
    },

    { title: 'Get high enough to grab the flag', 
      comments: '2 comments',
      time:       '3h ago',
      category:   'lifestyle',
      color:      'green',
      image:      'img/forest.jpg'
    },

    { title: 'Find the Princess', 
      comments: '113 comments',
      time:       '3h ago',
      category:   'adventure',
      color:      'red',
      image:      'img/china.jpg'
    },

    { title: 'Fight an evil turtle', 
      comments: '11 comments',
      time:       '6h ago',
      category:   'finance',
      color:      'blue',
      image:      'img/ibm.png'
    },

    { title: 'Climb some vines', 
      comments: '1 comment',
      time:       '6h ago',
      category:   'lifestyle',
      color:      'green',
      image:      'img/mountains.jpg'
    }
  ];


  // Create and load the Modal
  $ionicModal.fromTemplateUrl('new-article.html', function(modal) {
    $scope.articleModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });

  // Called when the form is submitted
  $scope.createArticle = function(article) {
    $scope.articles.push({
      title:      article.title,
      comments:   article.comments,
      time:       article.time,
      category:   article.category,
      color:      article.color,
      image:      article.image
    });
    $scope.articleModal.hide();
    article.title = "";
  };

  // Open our new task modal
  $scope.newArticleOpen = function() {
    $scope.articleModal.show();
  };

  // Close the new task modal
  $scope.closeNewArticle = function() {
    $scope.articleModal.hide();
  };

});
